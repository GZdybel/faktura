/**
 * Created by Gosia on 07/10/2015.
 */
public class produkt {

    private String name;
    private enum entitiy{tons, meters, item}
    private float entitiyNetto;
    private float entityBrutto;
    private int quantity;
    private float netto;
    private float brutto;
    private int procent;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getEntitiyNetto() {
        return entitiyNetto;
    }

    public void setEntitiyNetto(float entitiyNetto) {
        this.entitiyNetto = entitiyNetto;
    }

    public float getEntityBrutto() {
        return entityBrutto;
    }

    public void setEntityBrutto(float entityBrutto) {
        this.entityBrutto = entityBrutto;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public float getNetto() {
        return netto;
    }

    public void setNetto(float netto) {
        this.netto = netto;
    }

    public float getBrutto() {
        return brutto;
    }

    public void setBrutto(float brutto) {
        this.brutto = brutto;
    }

    public int getProcent() {
        return procent;
    }

    public void setProcent(int procent) {
        this.procent = procent;
    }

    public produkt(String name, float entitiyNetto, float entityBrutto, int quantity, float netto, float brutto, int procent) {
        this.name = name;
        this.entitiyNetto = entitiyNetto;
        this.entityBrutto = entityBrutto;
        this.quantity = quantity;
        this.netto = netto;
        this.brutto = brutto;
        this.procent = procent;
    }
}