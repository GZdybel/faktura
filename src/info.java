import java.util.Calendar;

/**
 * Created by Gosia on 07/10/2015.
 */
public class info {

    private Calendar rightNow;
    private int tillpay;
    private int tillpaydate;
    private float netto;
    private float brutto;
    private final int invoiceNumber;

    public Calendar getRightNow() {
        return rightNow;
    }

    public void setRightNow(Calendar rightNow) {
        this.rightNow = rightNow;
    }

    public int getTillpay() {
        return tillpay;
    }

    public void setTillpay(int tillpay) {
        this.tillpay = tillpay;
    }

    public int getTillpaydate() {
        return tillpaydate;
    }

    public void setTillpaydate(int tillpaydate) {
        this.tillpaydate = tillpaydate;
    }

    public float getNetto() {
        return netto;
    }

    public void setNetto(float netto) {
        this.netto = netto;
    }

    public float getBrutto() {
        return brutto;
    }

    public void setBrutto(float brutto) {
        this.brutto = brutto;
    }

    public int getInvoiceNumber() {
        return invoiceNumber;
    }

    public info(int invoiceNumber, Calendar rightNow, int tillpay, int tillpaydate, float netto, float brutto) {
        this.invoiceNumber = invoiceNumber;
        this.rightNow = rightNow;
        this.tillpay = tillpay;
        this.tillpaydate = tillpaydate;
        this.netto = netto;
        this.brutto = brutto;
    }
}
